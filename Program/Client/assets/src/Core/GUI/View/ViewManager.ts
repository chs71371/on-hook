import { UIRoot } from '../UIRoot';
import { View, ViewState } from './View';
import { Func } from '../../Assist/Func';
import { IDict, Dict } from '../../Assist/Dict';
import { UIAssetLoader } from '../../Loader/UIAssetLoader';
import { UtilTool } from '../../Assist/UtilTool';

export class ViewManager
{
    private static __instance: ViewManager = null;
    public static get Instance(): ViewManager
    {
        if (null == this.__instance)
        {
            this.__instance = new ViewManager();
        }
        return this.__instance;
    }

    private mDynamicRoot: cc.Node = null;
    private mPopupRoot: cc.Node = null;

    private mFrameRoot: cc.Node = null;
    private mPageRoot: cc.Node = null;

    private mCurPageViews: IDict<string, View> = null;
    private mCurFixedViews: IDict<string, View> = null;

    constructor()
    {
    }

    public Initialize()
    {
        this.mDynamicRoot = UIRoot.Instance.DynamicRoot;
        this.mPopupRoot = UIRoot.Instance.PopupRoot;

        this.mCurPageViews = Dict.CreateStringDict<View>();
        this.mCurFixedViews = Dict.CreateStringDict<View>();
    }

    public Update(dt)
    {
        if (this.mCurFixedViews != null)
        {
            for (let i = 0; i < this.mCurFixedViews.Count(); i++)
            {
                var rView = this.mCurFixedViews.At(i);
                if (rView != null)
                {
                    rView.Update(dt);
                }
            }
        }
        if (this.mCurPageViews != null)
        {
            for (let i = 0; i < this.mCurPageViews.Count(); i++)
            {
                var rView = this.mCurPageViews.At(i);
                if (rView != null)
                {
                    rView.Update(dt);
                }
            }
        }
    }

    public async Open(rViewName: string, rViewState: ViewState, rOpenCompleted: Func = null): Promise<View>
    {
        cc.log("Open ui: " + rViewName);
        this.MaybeCloseTopView(rViewState);
        var rViewPrefab = await UIAssetLoader.Instance.Load(rViewName);
        if (rViewPrefab == null)
        {
            cc.error("Cannot find prefab: " + rViewName);
        }
        return await this.OpenView(rViewName, rViewPrefab, rViewState, rOpenCompleted);
    }

    private async OpenView(rViewName: string, rViewPrefab: cc.Prefab, rViewState: ViewState, rOpenCompleted: Func): Promise<View>
    {
        var rPromise = new Promise<View>((resolve, reject) => 
        {
            if (rViewPrefab == null)
            {
                resolve(null);
                if (rOpenCompleted != null)
                    rOpenCompleted.Call();
                return;
            }

            //把View的GameObject结点加到rootCanvas下
            var rViewGo: cc.Node = null;
            switch (rViewState)
            {
                case ViewState.Fixing:
                    rViewGo = UtilTool.AddChild(rViewPrefab, this.mDynamicRoot);
                    break;
                case ViewState.Popup:
                    rViewGo = UtilTool.AddChild(rViewPrefab, this.mPopupRoot);
                    break;
                case ViewState.Frame:
                    rViewGo = UtilTool.AddChild(rViewPrefab, this.mFrameRoot);
                    break;
                case ViewState.Page:
                    rViewGo = UtilTool.AddChild(rViewPrefab, this.mPageRoot);
                    break;
            }

            var rView = View.CreateView(rViewGo);
            resolve(rView);
        });
        return rPromise;
    }

    private MaybeCloseTopView(rViewState: ViewState)
    {
        var rView = this.mCurPageViews.LastValue();
        if (rView == null) return;

        var rViewGUID = this.mCurPageViews.LastKey();
        if (rViewState == ViewState.Page)
        {
            this.mCurPageViews.Remove(rViewGUID);
            rView.Close();
            rView = null;
        }
    }
}
