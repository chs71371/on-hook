import ViewComponent from './ViewComponent';

export enum ViewState
{
    Fixing,
    Frame,
    Popup,
    Page,
}

export class View
{
    public GUID: string = "";
    public ViewName: string = "";
    public CurState: ViewState = ViewState.Fixing;

    public ViewNode: cc.Node = null;
    
    constructor()
    {
    }

    public static CreateView(rViewGo: cc.Node): View
    {
        var rView = new View();
        rView.ViewNode = rViewGo;
        return rView;
    }

    public Update(dt)
    {
    }

    public Close()
    {
    }
}