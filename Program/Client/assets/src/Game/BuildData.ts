import { RofbuildTableRow, RofscienceTableRow } from "./Rof/RofElements";
import { RofManager } from "./Rof/RofManager";
import { GoldData } from "./GameData";



export class ScienceData
{
    public ConfigData: RofscienceTableRow = null;

    public IsActive: boolean = false;

    public constructor(id: any)
    {
        this.ConfigData = RofManager.Singleton.ScienceTable.GetDataByID(id);
    }
}




export class BuildData
{
    public ConfigData: RofbuildTableRow = null;

    private mAdditionPool: ScienceData[] = [];

    private mLevel: number = 0;

    private mCost: GoldData;

    private mBuffMultiple: number = 1;

    private mProduce: GoldData;

    private mBaseProduce: GoldData;

    public constructor(id: any)
    {
        this.ConfigData = RofManager.Singleton.BuildTable.GetDataByID(id);
        this.mBaseProduce = GoldData.GetDataByString(this.ConfigData.GetBaseProduce());
        this.mProduce = this.mBaseProduce;
        this.mCost = GoldData.GetDataByString(this.ConfigData.GetUnlockCosts());
    }


    public AddLevel(value: number)
    {
        this.mLevel += value;

        this.mProduce = GoldData.GetMulValue(this.mBaseProduce, this.mLevel);

        this.mCost = GoldData.GetMulValue(this.mCost, (this.mLevel + 4) / (this.mLevel + 0.4));
    }

    public GetLevel(): number
    {
        return this.mLevel;
    }

    public GetCost(): GoldData
    {
        return this.mCost;
    }

    public AddBuff(data: ScienceData)
    {
        this.mAdditionPool.push(data);
        this.mBuffMultiple = 0;
        this.mAdditionPool.forEach(element =>
        {
            this.mBuffMultiple += element.ConfigData.GetUpMultiple();
        });

        this.mProduce = GoldData.GetMulValue(this.mProduce, this.mBuffMultiple);
    }


    public GetCurrentGoldRate(): GoldData
    {
        return this.mProduce;
    }

    public GetProduceByLevel(level: number)
    {
        return GoldData.GetMulValue(this.mBaseProduce, level * this.mBuffMultiple);
    }

}
