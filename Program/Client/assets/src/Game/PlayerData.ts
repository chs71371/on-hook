import { GoldData } from "./GameData";
import { RofManager } from "./Rof/RofManager";
import { ScienceData } from "./BuildData";


export class PlayerData 
{

    private mLevel: number = 1;

    private mProduce: GoldData;

    private mBaseProduce: GoldData;

    private mCost: GoldData;

    private mAdditionPool: ScienceData[] = [];

    private mBuffMultiple: number=1;

    public constructor()
    {
        this.mCost = GoldData.GetDataByString(RofManager.Singleton.PlayerTable.GetDataByID(1).GetUnlockCosts());
        this.mBaseProduce = GoldData.GetDataByString(RofManager.Singleton.PlayerTable.GetDataByID(1).GetBaseProduce());
        this.mProduce = this.mBaseProduce;

        //this.mProduce = new GoldData(10000, 0);
    }

    public GetLevel()
    {
        return this.mLevel;
    }

    public AddLevel()
    {
        this.mLevel++;

        //当前等级的产出
        this.mProduce = GoldData.GetMulValue(this.mBaseProduce, this.mLevel);

        //下一等级的花费
        this.mCost = GoldData.GetMulValue(this.mCost, ((this.mLevel + 4) / (this.mLevel + 0.4)));
    }

    public AddBuff(data: ScienceData)
    {
        this.mAdditionPool.push(data);
        this.mBuffMultiple = 0;
        this.mAdditionPool.forEach(element =>
        {
            this.mBuffMultiple += element.ConfigData.GetUpMultiple();
        });

        this.mProduce = GoldData.GetMulValue(this.mProduce, this.mBuffMultiple);
    }


    public GetBaseProduce()
    {
        return this.mBaseProduce;
    }

    public GetProduce()
    {
        return this.mProduce;
    }

    public GetProduceByLevel(level: number)
    {
        return GoldData.GetMulValue(this.mBaseProduce, level * this.mBuffMultiple);
    }

    public GetCost()
    {
        return this.mCost;
    }

}
