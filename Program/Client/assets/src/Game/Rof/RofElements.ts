import { ByteBuffer } from '../../Core/Assist/ByteBuffer';
import { IRofBase, RofMultiLanguage } from './RofManager';
export class RofleadRow implements IRofBase
{
private mID : number = 0;
private mBaseProduce : string = "";
private mUnlockCosts : string = "";
private mIcon : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mBaseProduce= rData.ReadString();
this.mUnlockCosts= rData.ReadString();
this.mIcon= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetBaseProduce() : string { return this.mBaseProduce }
public GetUnlockCosts() : string { return this.mUnlockCosts }
public GetIcon() : string { return this.mIcon }
}
export class RofbuildTableRow implements IRofBase
{
private mID : number = 0;
private mBuildName : string = "";
private mBaseProduce : string = "";
private mUnlockCosts : string = "";
private mBuildIcon : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mBuildName= rData.ReadString();
this.mBaseProduce= rData.ReadString();
this.mUnlockCosts= rData.ReadString();
this.mBuildIcon= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetBuildName() : string { return this.mBuildName }
public GetBaseProduce() : string { return this.mBaseProduce }
public GetUnlockCosts() : string { return this.mUnlockCosts }
public GetBuildIcon() : string { return this.mBuildIcon }
}
export class RofscienceTableRow implements IRofBase
{
private mID : number = 0;
private mName : string = "";
private mUnlockType : number = 0;
private mUnLockBuild : number = 0;
private mNeetCount : number = 0;
private mNeetGoldNum : string = "";
private mUpMultiple : number = 0;
private mIcon : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mName= rData.ReadString();
this.mUnlockType = rData.ReadInt32();
this.mUnLockBuild = rData.ReadInt32();
this.mNeetCount = rData.ReadInt32();
this.mNeetGoldNum= rData.ReadString();
this.mUpMultiple = rData.ReadFloat64();
this.mIcon= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetName() : string { return this.mName }
public GetUnlockType() : number { return this.mUnlockType }
public GetUnLockBuild() : number { return this.mUnLockBuild }
public GetNeetCount() : number { return this.mNeetCount }
public GetNeetGoldNum() : string { return this.mNeetGoldNum }
public GetUpMultiple() : number { return this.mUpMultiple }
public GetIcon() : string { return this.mIcon }
}
export class RofExampleRow implements IRofBase
{
private mID : number = 0;
private mValue1 : number = 0;
private mValue2 : string = "";
private mValue3 : number = 0;
private mValue4 : number = 0;
private mValue5 : string = "";
private mValue6 : number = 0;
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mValue1 = rData.ReadInt32();
for (let i = 0; i < 8; i++) {let temp = rData.ReadUInt8().toString(16);if (temp.length < 2){ temp = "0" + temp;}this.mValue2 += temp;}
this.mValue3 = rData.ReadFloat32();
this.mValue4 = rData.ReadFloat64();
this.mValue5= rData.ReadString();
this.mValue6 = rData.ReadInt32();
}
public GetID() : number { return this.mID }
public GetValue1() : number { return this.mValue1 }
public GetValue2() : string { return this.mValue2 }
public GetValue3() : number { return this.mValue3 }
public GetValue4() : number { return this.mValue4 }
public GetValue5() : string { return this.mValue5 }
public GetValue6() : string { return RofMultiLanguage.GetMultiLanguage(this.mValue6) }
}
export class RofLanguageRow implements IRofBase
{
private mID : number = 0;
private mChinese : string = "";
private mEnglish : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mChinese= rData.ReadString();
this.mEnglish= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetChinese() : string { return this.mChinese }
public GetEnglish() : string { return this.mEnglish }
}
export class RofParameterRow implements IRofBase
{
private mID : number = 0;
private mParam1 : number = 0;
private mParam2 : string = "";
public ReadBody(rData : ByteBuffer)
{
this.mID = rData.ReadInt32();
this.mParam1 = rData.ReadInt32();
this.mParam2= rData.ReadString();
}
public GetID() : number { return this.mID }
public GetParam1() : number { return this.mParam1 }
public GetParam2() : string { return this.mParam2 }
}
