import { ByteBuffer } from "../../Core/Assist/ByteBuffer";
import { Func } from "../../Core/Assist/Func";
import { RofExampleRow, RofLanguageRow, RofbuildTableRow, RofscienceTableRow, RofleadRow } from './RofElements';

export interface IRofBase
{
    ReadBody(rData: ByteBuffer);
}

var arrayBufferHandler = function (item, callback)
{
    var url = item.url;
    var xhr = cc.loader.getXMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.onload = function (oEvent)
    {
        var arrayBuffer = xhr.response;
        if (arrayBuffer)
        {
            var result = new Uint8Array(arrayBuffer);
            // 任何需要的处理
            callback(null, result);
        }
    }
    xhr.send(null);
};

export class RofTable<T extends IRofBase>
{
    private mPath: string;
    private mColNum: number;
    private mRowNum: number;
    private mIDMap: { [key: number]: T };
    private mRowMap: { [key: number]: number };

    constructor(private mTemplate: { new(): T; }, strPath: string)
    {
        RofManager.MaxCount++;
        this.mPath = strPath;
        this.mColNum = 0;
        this.mRowNum = 0;
        this.mIDMap = {};
        this.mRowMap = {};
        let strUrl = cc.url.raw(this.mPath)
        var that = this
        cc.loader.load({ url: strUrl, type: "bytes" }, function (err, rData)
        {
            if (err)
            {
                cc.error(err)
                return
            }
            that.Analysis(rData)
        })
    }

    private Analysis(rData: Uint8Array): void
    {
        let rBuffer = new ByteBuffer(rData.buffer);
        rBuffer.LittleEndian = false;
        rBuffer.Offset = 64;

        this.mRowNum = rBuffer.ReadInt32();
        this.mColNum = rBuffer.ReadInt32();

        //解析头
        for (let i = 0; i < this.mColNum; i++)
        {
            let nNameLen = rBuffer.ReadUInt8();
            rBuffer.Offset += nNameLen + 2;
        }
        //解析行
        for (let i = 0; i < this.mRowNum; i++)
        {
            let nID = rBuffer.ReadInt32()
            rBuffer.Offset -= 4;
            let obj = new this.mTemplate();
            obj.ReadBody(rBuffer);
            this.mIDMap[nID] = obj;
            this.mRowMap[i] = nID;
        }
        RofManager.CurCount++;
    }

    public GetData(): { [key: number]: T }
    {
        return this.mIDMap;
    }

    public GetDataByID(nID: number): T
    {
        return this.mIDMap[nID];
    }

    public GetDataByRow(nIndex: number): T
    {
        if (this.mRowMap[nIndex] == null)
        {
            return null;
        }
        let nID = this.mRowMap[nIndex];
        return this.mIDMap[nID];
    }

    public GetRowNum(): number
    {
        return this.mRowNum;
    }

    public GetColNum(): number
    {
        return this.mColNum;
    }

}

export class RofMultiLanguage
{
    static GetMultiLanguage(nID: number): string
    {
        return RofManager.Singleton.LanguageTable.GetDataByID(nID).GetChinese()
    }
}


export class RofManager
{
    static CurCount: number = 0;
    static MaxCount: number = 0;
    static mSingleton: RofManager;

    static get Singleton(): RofManager
    {
        if (this.mSingleton == null)
        {
            this.mSingleton = new RofManager()
        }
        return this.mSingleton;
    }

    static isLoadFinish(): boolean
    {
        if (this.CurCount == this.MaxCount)
        {
            return true
        }
        return false
    }

    public init(): boolean
    {
        cc.loader.addLoadHandlers({
            'bytes': arrayBufferHandler
        });
        return true
    }

    public readonly PlayerTable: RofTable<RofleadRow> = new RofTable<RofleadRow>(RofleadRow, "resources/rofs/Roflead.bytes");

    public readonly BuildTable: RofTable<RofbuildTableRow> = new RofTable<RofbuildTableRow>(RofbuildTableRow, "resources/rofs/RofbuildTable.bytes");
    public readonly ScienceTable: RofTable<RofscienceTableRow> = new RofTable<RofscienceTableRow>(RofscienceTableRow, "resources/rofs/RofscienceTable.bytes");
    public readonly LanguageTable: RofTable<RofLanguageRow> = new RofTable<RofLanguageRow>(RofLanguageRow, "resources/rofs/RofLanguage.bytes");

}

