import { BuildData, ScienceData } from "./BuildData";
import { NumberDict } from "../Core/Assist/Dict";
import { PlayerData } from "./PlayerData";


export enum GoldUnit
{
    None = 0,
    k = 1,
    m = 2,
    b = 3,
    t = 4,
}

export class GoldData
{
    public Value: number = 0;
    public Unit: GoldUnit = 0;

    public get ShowValue(): string
    {
        var suffix = "";

        var strValue = "";

        switch (this.Unit)
        {
            case GoldUnit.None:
                suffix = "";
                strValue = this.Value.toFixed();
                break;
            case GoldUnit.k:
                suffix = "k";
                strValue = this.Value.toFixed(3);
                break;
            case GoldUnit.m:
                suffix = "m";
                strValue = this.Value.toFixed(6);
                break;
            case GoldUnit.b:
                suffix = "b";
                strValue = this.Value.toFixed(6);
                break;
            case GoldUnit.t:
                suffix = "t";
                strValue = this.Value.toFixed(9);
                break;
        }

        return strValue + suffix;
    }


    public ShowValueForIgnoreDecimal(): string
    {
        return this.Value.toFixed() + this.SuffixUnit();
    }


    public SuffixUnit(): string
    {
        var suffix = "";
        switch (this.Unit)
        {
            case GoldUnit.None:
                suffix = "";

                break;
            case GoldUnit.k:
                suffix = "k";

                break;
            case GoldUnit.m:
                suffix = "m";

                break;
            case GoldUnit.b:
                suffix = "b";

                break;
            case GoldUnit.t:
                suffix = "t";
                break;
        }

        return suffix;
    }

    public constructor(rValue: any, rUnit: GoldUnit)
    {
        this.Value = rValue;
        this.Unit = rUnit;
    }

    public static GetDataByString(str: string): GoldData
    {
        var strArr = str.split(';');

        if (strArr.length == 1)
        {
            return this.CheckChangeUnit(new GoldData(parseInt(strArr[0]), GoldUnit.None));
        }

        var unit = GoldUnit.None;

        switch (strArr[1])
        {
            case "k":
                unit = GoldUnit.k;
                break;
            case "m":
                unit = GoldUnit.m;
                break;
            case "b":
                unit = GoldUnit.b;
                break;
            case "t":
                unit = GoldUnit.t;
                break;
            default:
                unit = GoldUnit.None;
                break;
        }

        return this.CheckChangeUnit(new GoldData(parseInt(strArr[0]), unit));
    }

    public static GetMulValue(data: GoldData, multiple: number): GoldData
    {
        var newData = new GoldData(data.Value, data.Unit);

        newData.Value = newData.Value * multiple;

        this.CheckChangeUnit(newData);

        return newData;
    }


    public static CheckChangeUnit(data: GoldData): GoldData
    {
        if (data.Value <= 0)
        {
            return;
        }

        while (data.Value < 1)
        {
            data.Unit -= 1;
            data.Value *= 1000;
            if (data.Value > 1 || data.Unit == 0)
            {
                return data;
            }
        }

        var newValue = data.Value / 1000;

        if (newValue > 1)
        {
            data.Value = parseFloat(newValue.toFixed(6));
            data.Unit += 1;
        }

        return data;
    }


    public static Compare(a: GoldData, b: GoldData): number
    {
        var dec = a.Unit - b.Unit;
        var decAbs = Math.abs(dec);
        var mul = 1;

        if (a.Value > 0 && dec > 2)
        {
            return 1;
        }

        if (b.Value > 0 && dec < -2)
        {
            return -1;
        }

        for (let index = 0; index < decAbs; index++)
        {
            if (dec > 0)
            {
                mul *= 1000;
            } else
            {
                mul /= 1000;
            }
        }

        var newValue = a.Value * mul;

        if (newValue > b.Value)
        {
            return 1;
        } else if (newValue < b.Value)
        {
            return -1;
        } else
        {
            return 0;
        }

    }

    public AddValue(data: GoldData)
    {
        var unitCount = data.Unit - this.Unit;

        if (unitCount == 0)
        {
            this.Value += data.Value;

        }
        else if (unitCount > 0)
        {
            var mul = 1;
            for (let index = 0; index < unitCount; index++)
            {
                mul *= 1000;
            }

            this.Value += data.Value * mul;
        }

        if (unitCount == -1)
        {
            var addvalue = data.Value * 0.001;

            this.Value = parseFloat((this.Value + addvalue).toFixed(6));
        }

        GoldData.CheckChangeUnit(this);
    }

}

export enum BuyUnit
{
    One = 0,
    Ten,
    Hun,
    Max
}


export class GameData
{
    private static _Instance: GameData;

    public static get Instance(): GameData
    {
        if (this._Instance == null)
        {
            this._Instance = new GameData();
        }
        return this._Instance;
    };

    private mPlayer: PlayerData;

    private mAllGold: GoldData = new GoldData(0, GoldUnit.None);

    private mAllGoldRate: GoldData = new GoldData(0, GoldUnit.None);

    private mAllBuild: NumberDict<BuildData> = new NumberDict<BuildData>();

    private mAllScience: NumberDict<ScienceData> = new NumberDict<ScienceData>();

    //public AllBuildValue: GoldData = new GoldData(0, GoldUnit.None);

    public CurBuyUnit: BuyUnit = 0;


    public Init() 
    {
        this.mPlayer = new PlayerData();
    }

    public GetPlayer()
    {
        return this.mPlayer;
    }

    public GetBuilds()
    {
        return this.mAllBuild;
    }

    public GetScience()
    {
        return this.mAllScience;
    }

    public AddCurrentGold(data: GoldData)
    {
        this.mAllGold.AddValue(data);
    }

    public ReduceCurrentGold(data: GoldData)
    {
        var newData = new GoldData(-data.Value, data.Unit);
        this.mAllGold.AddValue(newData);
    }

    public GetCurrentClickGold(): GoldData
    {
        return this.mPlayer.GetProduce();
    }

    public GetCurrentGold(): GoldData
    {
        return this.mAllGold;
    }

    public GetCurrentGoldRate(): GoldData
    {
        return this.mAllGoldRate;
    }

    public AddBuild(id: number, level: number)
    {
        if (this.mAllBuild.ContainsKey(id))
        {
            this.mAllBuild.Get(id).AddLevel(level);
        }
        else
        {
            var build = new BuildData(id);
            build.AddLevel(1);
            this.mAllBuild.Add(id, build);
        }

        this.FreshGoldRate();
    }

    public AddScience(data: ScienceData)
    {
        var science = data;

        switch (science.ConfigData.GetUnlockType())
        {
            case 1:
                this.mAllScience.Add(science.ConfigData.GetID(), science);

                this.mAllBuild.GetKeys().forEach(element =>
                {
                    if (element == science.ConfigData.GetUnLockBuild())
                    {
                        this.mAllBuild.Get(element).AddBuff(science);
                    }
                });
                this.FreshGoldRate();
                break;
            case 2:
                this.mPlayer.AddBuff(science);
                break;
        }

    }


    private FreshGoldRate()
    {
        var newData = new GoldData(0, GoldUnit.None);
        this.mAllBuild.GetKeys().forEach(element =>
        {
            newData.AddValue(this.mAllBuild.Get(element).GetCurrentGoldRate());
        });

        this.mAllGoldRate = newData;
    }


    public CanPay(data: GoldData): boolean
    {
        if (GoldData.Compare(this.mAllGold, data) == -1)
        {
            return false;
        } else
        {
            return true;
        }
    }

}
