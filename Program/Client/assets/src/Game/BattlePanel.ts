import BattleManager from "./BattleManager";
import { GameData, BuyUnit } from "./GameData";
import { ResourceLoader } from "../Core/Loader/ResourceLoader";
import { GameObjectPool } from "../Core/Assist/GameObjectPool";
import { RofManager } from "./Rof/RofManager";

import { StringDict, NumberDict } from "../Core/Assist/Dict";
import { BuildData, ScienceData } from "./BuildData";
import BuildAndScienceElementView from "./BuildAndScienceElementView";
import GameMain from "./GameMain";



export class ScienceGroup
{
    private mScienceArr: BuildAndScienceElementView[] = [];

    private mIndex = 0;

    public AddScience(data: BuildAndScienceElementView)
    {
        this.mScienceArr.push(data);
    }

    public CompletaScience()
    {
        this.mScienceArr[this.mIndex].node.active = false;

        if (this.mIndex < this.mScienceArr.length)
        {
            this.mIndex += 1;
            this.mScienceArr[this.mIndex].node.active = true;
        }

    }
}


const { ccclass, property } = cc._decorator;

@ccclass
export default class BattlePanel extends cc.Component
{



    @property(cc.Node)
    mShowGoldElementArea: cc.Node = null;

    @property(cc.Node)
    mActionNode: cc.Node = null;
    @property(cc.Label)
    mTitle: cc.Label = null;
    @property(cc.Label)
    mAllGold: cc.Label = null;

    //操作界面
    @property(cc.Node)
    mOperationView: cc.Node = null;

    //分页
    @property(cc.Node)
    mSciencePanel: cc.Node = null;

    @property(cc.Node)
    mBuildPanel: cc.Node = null;

    @property(cc.Label)
    mBuyUtilButtonLabel: cc.Label = null;

    //建筑栏
    @property(cc.Label)
    mBuildAllGold: cc.Label = null;

    @property(cc.Node)
    mBuildContent: cc.Node = null;

    //科技分页子功能
    @property(cc.Label)
    mScienceUnLockProgress: cc.Label = null;
    @property(cc.Node)
    mPlayerScienceContent: cc.Node = null;
    @property(cc.Node)
    mScienceContent: cc.Node = null;
    @property(cc.Node)
    mOneKeyUnLockButton: cc.Node = null;

    mPlayer: BuildAndScienceElementView = null;

    mBuildDict: NumberDict<BuildAndScienceElementView> = new NumberDict<BuildAndScienceElementView>();

    mScienceGroupDict: StringDict<ScienceGroup> = new StringDict<ScienceGroup>();


    start()
    {
        this.mActionNode.on(cc.Node.EventType.TOUCH_END, this.OnProduceResouceClick, this);
    }




    public Init()
    {

        var obj = GameMain.Instance.BuildPool.Alloc();
        obj.setParent(this.mPlayerScienceContent);
        obj.position = new cc.Vec2(0, 0);
        this.mPlayer = obj.getComponent(BuildAndScienceElementView);
        this.mPlayer.SetViewDataByPlayer();


        for (let index = 0; index < RofManager.Singleton.BuildTable.GetRowNum(); index++)
        {
            let element = RofManager.Singleton.BuildTable.GetDataByRow(index);

            var obj = GameMain.Instance.BuildPool.Alloc();
            obj.setParent(this.mBuildContent);

            var script = obj.getComponent(BuildAndScienceElementView);
            this.mBuildDict.Add(element.GetID(), script);
            script.SetViewDataByBuild(new BuildData(element.GetID()));
        }


        var curGroupId = "";
        for (let index = 0; index < RofManager.Singleton.ScienceTable.GetRowNum(); index++)
        {
            let element = RofManager.Singleton.ScienceTable.GetDataByRow(index);

            var obj = GameMain.Instance.BuildPool.Alloc();
            obj.setParent(this.mScienceContent);

            var groupId = element.GetID().toString().substring(1, 3);

            if (groupId != curGroupId)
            {

                curGroupId = groupId;
                var scienceGroup = new ScienceGroup();
                this.mScienceGroupDict.Add(curGroupId, scienceGroup);
                obj.active = true;
            } else
            {
                obj.active = false;
            }

            var script = obj.getComponent(BuildAndScienceElementView);
            script.SetViewDataByScience(new ScienceData(element.GetID()));
            this.mScienceGroupDict.Get(curGroupId).AddScience(script);
        }

    }


    private OnShowTable(event, customEventData)
    {
        if (this.mOperationView.y != -336)
        {
            var move = cc.moveTo(1, new cc.Vec2(0, -336)).easing(cc.easeExponentialOut());
            this.mOperationView.runAction(move);
        }

        this.mBuildPanel.active = false;
        this.mSciencePanel.active = false;

        switch (customEventData)
        {
            case "0":
                this.mSciencePanel.active = true;

                break;
            case "1":
                this.mBuildPanel.active = true;

                break;
            case "2":

                break;
            case "3":

                break;
        }
    }






    private OnProduceResouceClick(event)
    {

        var mousePos = event.touch.getLocation();

        BattleManager.Instance.AddClickRes();

        var goldData = GameData.Instance.GetCurrentClickGold();

        var obj = GameMain.Instance.ClickElementPool.Alloc();
        obj.active = true;
        obj.opacity = 255;
        obj.setParent(this.mShowGoldElementArea);
        obj.getChildByName("Txt_Gold").getComponent(cc.Label).string = "$" + goldData.ShowValue;


        obj.x = mousePos.x;
        obj.y = mousePos.y;


        var spawn = cc.spawn(
            cc.moveTo(1, new cc.Vec2(obj.x, obj.y + 100)).easing(cc.easeExponentialOut())
            , cc.fadeOut(1));

        obj.runAction(spawn);

        this.scheduleOnce(() =>
        {
            GameMain.Instance.ClickElementPool.Free(obj);
        }, 1);
    }


    public FreshPlayer()
    {
        this.mPlayer.SetViewDataByPlayer();
    }

    public FreshBuild(data: BuildData)
    {
        this.mBuildDict.Get(data.ConfigData.GetID()).SetViewDataByBuild(data);
    }


    public FreshScienceGroup(id: number)
    {
        var groupId = id.toString().substring(1, 3);
        this.mScienceGroupDict.Get(groupId).CompletaScience();
    }




    private OnClickChangeBuyUtil()
    {
        GameData.Instance.CurBuyUnit = (GameData.Instance.CurBuyUnit + 1) % 4;

        switch (GameData.Instance.CurBuyUnit)
        {
            case BuyUnit.One:
                this.mBuyUtilButtonLabel.string = "Buy X" + 1;
                break;
            case BuyUnit.Ten:
                this.mBuyUtilButtonLabel.string = "Buy X" + 10;
                break;
            case BuyUnit.Hun:
                this.mBuyUtilButtonLabel.string = "Buy X" + 100;
                break;
            case BuyUnit.Max:
                this.mBuyUtilButtonLabel.string = "Buy Max";
                break;
        }

    }

    private OnClosePanel()
    {
        var move = cc.moveTo(1, new cc.Vec2(0, -1064)).easing(cc.easeExponentialOut());
        this.mOperationView.runAction(move);
    }

    update(dt)
    {
        this.mAllGold.string = "净值：" + GameData.Instance.GetCurrentGold().ShowValue;

        this.mBuildAllGold.string = "建筑利润:$" + GameData.Instance.GetCurrentGoldRate().ShowValue + "/秒";
    }
}
