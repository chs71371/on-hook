import { RofbuildTableRow } from "./Rof/RofElements";
import { BuildData, ScienceData } from "./BuildData";
import { ResourceLoader } from "../Core/Loader/ResourceLoader";
import BattleManager from "./BattleManager";
import { GoldData, GameData } from "./GameData";
import { RofManager } from "./Rof/RofManager";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

enum ShowType
{
    Player,
    Build,
    Science,
}

@ccclass
export default class BuildAndScienceElementView extends cc.Component
{

    @property(cc.Label)
    mBuildName: cc.Label = null;

    @property(cc.Label)
    mGoldRata: cc.Label = null;

    @property(cc.Label)
    mLevel: cc.Label = null;

    @property(cc.Label)
    mCost: cc.Label = null;

    @property(cc.Label)
    mBuyLabel: cc.Label = null;

    @property(cc.Sprite)
    mIcon: cc.Sprite = null;

    @property(cc.Sprite)
    mBuyButton: cc.Sprite = null;

    @property(cc.Sprite)
    mBG: cc.Sprite = null;



    mBuildData: BuildData = null;

    mScienceData: ScienceData = null;

    mCostData: GoldData = null;

    mShowType: ShowType = ShowType.Player;


    onLoad() 
    {
        this.mBuyLabel.string = "购买";
    }

    public SetViewDataByPlayer()
    {
        this.mShowType = ShowType.Player;

        this.mCostData = GameData.Instance.GetPlayer().GetCost();

        var showLevel = GameData.Instance.GetPlayer().GetLevel() + 1;

        this.mLevel.string = "Lv." + showLevel;

        this.mBuildName.string = "主角名字";

        this.mGoldRata.string = "产值：" + GameData.Instance.GetPlayer().GetProduceByLevel(showLevel).ShowValueForIgnoreDecimal() + "/秒";

        this.mCost.string = "$" + this.mCostData.ShowValueForIgnoreDecimal();
    }


    public SetViewDataByBuild(data: BuildData)
    {
        if (data.GetLevel() >= 1)
        {
            this.mBuyLabel.string = "升级";
        }

        this.mShowType = ShowType.Build;
        this.mBuildData = data;
        this.mCostData = data.GetCost();

        var showLevel = data.GetLevel() + 1;

        this.mLevel.string = "Lv." + showLevel;
        this.mBuildName.string = data.ConfigData.GetBuildName();
        this.mGoldRata.string = "产值：" + data.GetProduceByLevel(showLevel).ShowValueForIgnoreDecimal() + "/秒";
        this.mCost.string = "$" + this.mCostData.ShowValueForIgnoreDecimal();

        if (data.ConfigData.GetBuildIcon() != "0")
        {
            ResourceLoader.Instance.CreatSprite(data.ConfigData.GetBuildIcon(), (o) =>
            {
                this.mIcon.spriteFrame = o;
            });
        }
    }

    public SetViewDataByScience(data: ScienceData)
    {
        this.mShowType = ShowType.Science;

        this.mScienceData = data;
        this.mCostData = GoldData.GetDataByString(data.ConfigData.GetNeetGoldNum());
        this.mCost.string = "$" + this.mCostData.ShowValueForIgnoreDecimal();
        this.mBuildName.string = data.ConfigData.GetName();

        this.mLevel.node.active = false;

        if (data.ConfigData.GetUnlockType() == 1)
        {
            if (data.ConfigData.GetUnLockBuild() != 0)
            {
                var targetBuildName = RofManager.Singleton.BuildTable.GetDataByID(data.ConfigData.GetUnLockBuild()).GetBuildName();
                this.mGoldRata.string = targetBuildName + "的收益X" + data.ConfigData.GetUpMultiple();
            }
        }
        else
        {
            this.mGoldRata.string = "玩家点击的收益X" + data.ConfigData.GetUpMultiple();
        }
    }


    public OnBuy()
    {
        if (!this.CanPay())
        {
            return;
        }

        switch (this.mShowType)
        {
            case ShowType.Player:
                BattleManager.Instance.BuyPlayerLevel();
                this.SetViewDataByPlayer();
                break;
            case ShowType.Build:
            
                BattleManager.Instance.BuyBuild(this.mBuildData);
                break;
            case ShowType.Science:

                BattleManager.Instance.BuyScience(this.mScienceData);
                break;
        }
    }


    private CanPay(): boolean
    {
        if (!GameData.Instance.CanPay(this.mCostData))
        {
            return false;
        }

        if (this.mShowType == ShowType.Science)
        {
            var targetLevel = 0;
            if (this.mScienceData.ConfigData.GetUnLockBuild() != 0)
            {
                var targetBuild = GameData.Instance.GetBuilds().Get(this.mScienceData.ConfigData.GetUnLockBuild());
                if (targetBuild == null)
                {
                    return false;
                }

                targetLevel = targetBuild.GetLevel();
            }
            else
            {
                targetLevel = GameData.Instance.GetPlayer().GetLevel();
            }

            if (targetLevel < this.mScienceData.ConfigData.GetNeetCount())
            {
                return false;
            }

        }



        return true;
    }

    update(dt)
    {
        if (this.mCostData != null)
        {
            if (this.CanPay())
            {
                this.mCost.node.color = cc.color(0, 0, 0, 255);
                this.mBuyButton.node.color = cc.color(255, 255, 255, 255);
                this.mBG.node.color = cc.color(255, 255, 255, 255);
                this.mBuyButton.getComponent(cc.Button).interactable = true;
            }
            else
            {
                this.mCost.node.color = cc.color(255, 0, 0, 255);
                this.mBuyButton.node.color = cc.color(126, 126, 126, 255);
                this.mBG.node.color = cc.color(126, 126, 126, 255);
                this.mBuyButton.getComponent(cc.Button).interactable = false;
            }
        }
    }

}
