import BattlePanel from "./BattlePanel";
import { GameData, GoldData, GoldUnit } from "./GameData";
import { BuildData, ScienceData } from "./BuildData";
import { PlayerData } from "./PlayerData";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BattleManager extends cc.Component
{

    public static Instance: BattleManager;

    @property(cc.Component)
    mBattlePanel: BattlePanel = null;



    onLoad() 
    {
        BattleManager.Instance = this;

    }

    public Init()
    {
        GameData.Instance.Init();
        this.mBattlePanel.Init();

        this.schedule(this.GoldUpdate, 1, cc.macro.REPEAT_FOREVER);
    }


    public AddClickRes()
    {
        GameData.Instance.AddCurrentGold(GameData.Instance.GetCurrentClickGold());
    }


    public BuyPlayerLevel()
    {
        GameData.Instance.ReduceCurrentGold(GameData.Instance.GetPlayer().GetCost());
        GameData.Instance.GetPlayer().AddLevel();
    }


    public BuyBuild(data: BuildData)
    {
        var cost = data.GetCost();

        GameData.Instance.ReduceCurrentGold(cost);

        GameData.Instance.AddBuild(data.ConfigData.GetID(), 1);

        this.mBattlePanel.FreshBuild(GameData.Instance.GetBuilds().Get(data.ConfigData.GetID()));
    }

    public BuyScience(data: ScienceData)
    {
        var cost = GoldData.GetDataByString(data.ConfigData.GetNeetGoldNum());
        GameData.Instance.ReduceCurrentGold(cost);

        GameData.Instance.AddScience(data);

        if (data.ConfigData.GetUnlockType() == 1)
        {
            this.mBattlePanel.FreshScienceGroup(data.ConfigData.GetID());
        } else if (data.ConfigData.GetUnlockType() == 2)
        {
            this.mBattlePanel.FreshPlayer();
        }
    }


    private GoldUpdate()
    {
        GameData.Instance.AddCurrentGold(GameData.Instance.GetCurrentGoldRate());
    }

}
