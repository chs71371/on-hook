package rof
import (
"encoding/binary"
"math"
)
type sRofDeckConfigRow struct {
mID int32
mValue1 int32
mValue2 string
mValue3 string
mValue4 string
}
func (pOwn *sRofDeckConfigRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue1 = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nValue2Len := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue2 = string(aBuffer[nOffset:nOffset+nValue2Len])
nOffset+=nValue2Len
nValue3Len := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue3 = string(aBuffer[nOffset:nOffset+nValue3Len])
nOffset+=nValue3Len
nValue4Len := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue4 = string(aBuffer[nOffset:nOffset+nValue4Len])
nOffset+=nValue4Len
return nOffset
}
func (pOwn *sRofDeckConfigRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofDeckConfigRow) GetValue1() int32 { return pOwn.mValue1 } 
func (pOwn *sRofDeckConfigRow) GetValue2() string { return pOwn.mValue2 } 
func (pOwn *sRofDeckConfigRow) GetValue3() string { return pOwn.mValue3 } 
func (pOwn *sRofDeckConfigRow) GetValue4() string { return pOwn.mValue4 } 
type sRofDeckConfigTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofDeckConfigRow
mRowMap map[int32]int32
}
func (pOwn *sRofDeckConfigTable) newTypeObj() iRofRow {return new(sRofDeckConfigRow)}
func (pOwn *sRofDeckConfigTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofDeckConfigTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofDeckConfigTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofDeckConfigRow)}
func (pOwn *sRofDeckConfigTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofDeckConfigTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofDeckConfigRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofDeckConfigTable) GetDataByID(aID int32) *sRofDeckConfigRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofDeckConfigTable) GetDataByRow(aIndex int32) *sRofDeckConfigRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofDeckConfigTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofDeckConfigTable) GetCols() int32 {return pOwn.mColNum}
type sRofExampleRow struct {
mID int32
mValue1 int32
mValue2 int64
mValue3 float32
mValue4 float64
mValue5 string
mValue6 int32
}
func (pOwn *sRofExampleRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue1 = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue2 = int64(binary.BigEndian.Uint64(aBuffer[nOffset:]))
nOffset+=8
pOwn.mValue3 = math.Float32frombits(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue4 = math.Float64frombits(binary.BigEndian.Uint64(aBuffer[nOffset:]))
nOffset+=8
nValue5Len := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mValue5 = string(aBuffer[nOffset:nOffset+nValue5Len])
nOffset+=nValue5Len
pOwn.mValue6 = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
return nOffset
}
func (pOwn *sRofExampleRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofExampleRow) GetValue1() int32 { return pOwn.mValue1 } 
func (pOwn *sRofExampleRow) GetValue2() int64 { return pOwn.mValue2 } 
func (pOwn *sRofExampleRow) GetValue3() float32 { return pOwn.mValue3 } 
func (pOwn *sRofExampleRow) GetValue4() float64 { return pOwn.mValue4 } 
func (pOwn *sRofExampleRow) GetValue5() string { return pOwn.mValue5 } 
func (pOwn *sRofExampleRow) GetValue6() int32 { return pOwn.mValue6 } 
type sRofExampleTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofExampleRow
mRowMap map[int32]int32
}
func (pOwn *sRofExampleTable) newTypeObj() iRofRow {return new(sRofExampleRow)}
func (pOwn *sRofExampleTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofExampleTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofExampleTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofExampleRow)}
func (pOwn *sRofExampleTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofExampleTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofExampleRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofExampleTable) GetDataByID(aID int32) *sRofExampleRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofExampleTable) GetDataByRow(aIndex int32) *sRofExampleRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofExampleTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofExampleTable) GetCols() int32 {return pOwn.mColNum}
type sRofLanguageRow struct {
mID int32
mChinese string
mEnglish string
}
func (pOwn *sRofLanguageRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nChineseLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mChinese = string(aBuffer[nOffset:nOffset+nChineseLen])
nOffset+=nChineseLen
nEnglishLen := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mEnglish = string(aBuffer[nOffset:nOffset+nEnglishLen])
nOffset+=nEnglishLen
return nOffset
}
func (pOwn *sRofLanguageRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofLanguageRow) GetChinese() string { return pOwn.mChinese } 
func (pOwn *sRofLanguageRow) GetEnglish() string { return pOwn.mEnglish } 
type sRofLanguageTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofLanguageRow
mRowMap map[int32]int32
}
func (pOwn *sRofLanguageTable) newTypeObj() iRofRow {return new(sRofLanguageRow)}
func (pOwn *sRofLanguageTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofLanguageTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofLanguageTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofLanguageRow)}
func (pOwn *sRofLanguageTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofLanguageTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofLanguageRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofLanguageTable) GetDataByID(aID int32) *sRofLanguageRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofLanguageTable) GetDataByRow(aIndex int32) *sRofLanguageRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofLanguageTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofLanguageTable) GetCols() int32 {return pOwn.mColNum}
type sRofParameterRow struct {
mID int32
mParam1 int32
mParam2 string
}
func (pOwn *sRofParameterRow) readBody(aBuffer []byte) int32 {
var nOffset int32
pOwn.mID = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mParam1 = int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
nParam2Len := int32(binary.BigEndian.Uint32(aBuffer[nOffset:]))
nOffset+=4
pOwn.mParam2 = string(aBuffer[nOffset:nOffset+nParam2Len])
nOffset+=nParam2Len
return nOffset
}
func (pOwn *sRofParameterRow) GetID() int32 { return pOwn.mID } 
func (pOwn *sRofParameterRow) GetParam1() int32 { return pOwn.mParam1 } 
func (pOwn *sRofParameterRow) GetParam2() string { return pOwn.mParam2 } 
type sRofParameterTable struct { 
mRowNum int32
mColNum int32
mIDMap  map[int32]*sRofParameterRow
mRowMap map[int32]int32
}
func (pOwn *sRofParameterTable) newTypeObj() iRofRow {return new(sRofParameterRow)}
func (pOwn *sRofParameterTable) setRowNum(aNum int32) {pOwn.mRowNum = aNum}
func (pOwn *sRofParameterTable) setColNum(aNum int32) {pOwn.mColNum = aNum}
func (pOwn *sRofParameterTable) setIDMap(aKey int32, aValue iRofRow) {pOwn.mIDMap[aKey] = aValue.(*sRofParameterRow)}
func (pOwn *sRofParameterTable) setRowMap(aKey int32, aValue int32) {pOwn.mRowMap[aKey] = aValue}
func (pOwn *sRofParameterTable) init(aPath string) bool {
pOwn.mIDMap = make(map[int32]*sRofParameterRow)
pOwn.mRowMap = make(map[int32]int32)
return analysisRof(aPath, pOwn)
}
func (pOwn *sRofParameterTable) GetDataByID(aID int32) *sRofParameterRow {return pOwn.mIDMap[aID]}
func (pOwn *sRofParameterTable) GetDataByRow(aIndex int32) *sRofParameterRow {
nID, ok := pOwn.mRowMap[aIndex]
if ok == false {return nil}
return pOwn.mIDMap[nID]
}
func (pOwn *sRofParameterTable) GetRows() int32 {return pOwn.mRowNum}
func (pOwn *sRofParameterTable) GetCols() int32 {return pOwn.mColNum}
